/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ht.mid_alg9;

import java.util.Scanner;

/**
 *
 * @author ACER
 */
public class ALG9 {
    public static void main(String[] args) {
        long start = System.nanoTime();
        
        Scanner kb = new Scanner(System.in);
        String str1 = kb.nextLine();
        String str2 = kb.nextLine();
        
        System.out.println("Input String");
        System.out.println("String1 + String2 = " + (str1 + str2));
        
        System.out.println();
        
        System.out.println("Converse String to Integer");
        int conint1 = Integer.parseInt(str1);
        int conint2 = Integer.parseInt(str2);
        System.out.println("Integer1 + Integer2 = " + (conint1 + conint2));
        
        long end = System.nanoTime();
        System.out.println("Running Time of Algorithm is " + (end - start) * 1E-9 + " secs.");
    }
}
